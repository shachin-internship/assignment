Matrix multiplication is used to multiply 2 matrix and test them using uunit testing.
It takes the rows , cols , the 2 input matrices and the output from the text file and check whether the output obtained from Matrix multiplication code is same as the outmatrix given.

Instructions:
Clone the repository.
Put all Files into a single directory.
Run the batch.bat file to test more than one file at a time.
Run the validate.c to test Matrix multiplication.
To run with different input use any one of the sample input text file and run using cmd prompt or create a new input text file.
To run the Python file run using validate.py give the input text file from the available sample text files as argument in cmd prompt.