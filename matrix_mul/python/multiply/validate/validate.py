import numpy as np
import matrix_multiply
import time
import get_from_txt
from sys import argv

def validate():
    #get matrices from text file
    matrixA = get_from_txt.a
    matrixB = get_from_txt.b
    sample_result = get_from_txt.r
    start_time = time.time()
    #do matrix multiplication
    result = matrix_multiply.multiply_matrices(matrixA , matrixB)
    print("--- {0:.2f} ms ---" .format ((time.time() - start_time) * 1000))
    #compare result matrix with expected matrix
    for i in range(len(matrixA)):
        for j in range(len(matrixB[0])):
            if sample_result[i][j] == result[i][j]:
                continue
            else:
                print("Output mismatch")
    print("Both outputs are same")

if __name__ == "__main__":
    validate()