import numpy as np
import validate
from sys import argv
#read the content from text file
f = open(validate.argv[1])
text = f.readlines()
f.close()

lines = []
[lines.append(line) for line in text]

a = lines[0].split(" ")
r1 , c1 , r2 , c2 = a[0] , a[1] , a[2] , a[3]
#create matrixA matrixB result
matA = lines[1].split(" ")
matB = lines[2].split(" ")
resM = lines[3].split(" ")
matA = list(map(int , matA[:-1]))
matB = list(map(int , matB[:-1]))
resMM = list(map(int , resM[:-1]))
resMM.append(int(resM[-1][:-1]))

matrixA = np.reshape(matA , (int(r1) , int(c1)))
matrixB = np.reshape(matB , (int(r2) , int(c2)))
resultM = np.reshape(resMM , (int(r1) , int(c2)))
#make the matrices as numpy array
a = np.array(matrixA)
b = np.array(matrixB)
r = np.array(resultM)