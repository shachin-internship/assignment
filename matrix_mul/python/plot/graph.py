import matplotlib.pyplot as plt
import pandas as pd

data = pd.read_csv("o2_opti.csv")

plt.plot(data.Name , data.Time_ijk , marker = "*")
plt.plot(data.Name , data.Time_ikj , marker = "*")
plt.plot(data.Name , data.Time_jki , marker = "*")
plt.plot(data.Name , data.Time_jik , marker = "*")
plt.plot(data.Name , data.Time_kij , marker = "*")
plt.plot(data.Name , data.Time_kji , marker = "*")
plt.plot(data.Name , data.Time_py , marker = "*")


plt.xlabel("matrix dim")
plt.ylabel("time(ms)")
plt.legend(['ijk' , 'ikj' , 'jki' , 'jik' , 'kij' , 'kji' ,'Python'])
plt.show()