//matrix multiplication using diffferent loops
#include<stdio.h>
#include<stdlib.h> 

void multiply_matrix_ijk(int row1 , int col1 , int row2 , int col2 , int** matrix1, int** matrix2 , int** result){
	
	int i , j , k, ij;
	
 	for(i=0; i < row1; i++) 
	{ 
		for(j=0; j < col2; j++) 
		{ 
	 		for(k=0; k<col1; k++)  
				result[i][j] += matrix1[i][k] * matrix2[k][j]; 
		} 
	} 
}

void multiply_matrix_ikj(int row1 , int col1 , int row2 , int col2 , int** matrix1, int** matrix2 , int** matrix3){
	
	
	int i , j , k;
	int temp_var;
	
	for(i=0; i < row1; i++) {
	 	for(k=0; k<col1; k++) {
	 		temp_var = matrix1[i][k];
			for(j=0; j < col2; j++) 
				matrix3[i][j] += temp_var * matrix2[k][j]; 		
		} 
	}
	 	
}

void multiply_matrix_jik(int row1 , int col1 , int row2 , int col2 , int** matrix1, int** matrix2 , int** result){
	
	
	
	int i , j , k, ij;
	
	
 	for(j=0; j < col2; j++) 
	{ 
		for(i=0; i < row1; i++) 
		{ 
	 		for(k=0; k<col1; k++)  
				result[i][j] += matrix1[i][k] * matrix2[k][j]; 
		} 
	} 
 		

}


void multiply_matrix_jki(int row1 , int col1 , int row2 , int col2 , int** matrix1, int** matrix2 , int** result){
	
	
	
	int i , j , k, ij;
	int temp_var;

 	for(j = 0 ; j < col2 ; j++){
		for(k = 0 ; k < col1 ; k++){
			temp_var = matrix2[k][j];
			for(i = 0 ; i < row1 ; i++)
				result[i][j] += matrix1[i][k] * temp_var;
		}
	}
 	
}

void multiply_matrix_kij(int row1 , int col1 , int row2 , int col2 , int** matrix1, int** matrix2 , int** matrix3){
	
	
	
	int i , j , k, ij;
	int temp_var;
 	for(k=0; k<col1; k++) {
		for(i=0; i < row1; i++) 
		{  
			temp_var = matrix1[i][k];
			for(j=0; j < col2; j++) 
				matrix3[i][j] += temp_var * matrix2[k][j];  
		} 
	}
}

void multiply_matrix_kji(int row1 , int col1 , int row2 , int col2 , int** matrix1, int** matrix2 , int** matrix3){
	
	
	
	int i , j , k, ij;
	int temp_var;
	 
 	for(k=0; k<col1 ; k++) {
 		for(j=0; j < col2 ; j++) {
 			temp_var = matrix2[k][j];
			for(i=0; i < row1 ; i++)  
				matrix3[i][j] += matrix1[i][k] * temp_var; 
		} 
	}
}

