#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include "multiply_matrices.c"

int check_matrix(char* z){
	
	FILE *fptr,*fptr1;
    char* filename = z;
    char ch;
    char c[10] , empty[1];
    empty[1] = '\0';
    fptr = fopen(filename, "r");
    if (fptr == NULL)
    {
        printf("Invaid filename \n");
        exit(0);
    }
    int i , j;
    int dim[4];
    int t=0;

	int	num=0;
	for(t=0;t<4;)
	{
		ch=fgetc(fptr);
			if(isspace(ch))
			{
				dim[t]=num;
				num=0;
				t++;
				
			}
			else
			{
				if(ch == '-'){
					printf("Row or Col less than zero");
					exit(0);
				}
				else{
				num =(num*10) +(ch-'0');
				}	
			}
	}
	
	
	
	
	int r1=dim[0],c1=dim[1],r2=dim[2],c2=dim[3];
	
//initializing matrixA
	int**  matrixA ;
	matrixA = (int **) malloc(sizeof(int *) * r1);
	for(i=0; i<r1; i++)  
 		matrixA[i] = (int *)malloc(sizeof(int) * c1);
 	for(i = 0 ; i < r1 ; i++){
		for(j = 0 ; j < c1 ; j++){
			matrixA[i][j] = 0;
		}
	}
//initializing matrixB
	int**  matrixB ;
	matrixB = (int **) malloc(sizeof(int *) * r2);
	for(i=0; i<r2; i++)  
 		matrixB[i] = (int *)malloc(sizeof(int) * c2);	
 	
 	for(i = 0 ; i < r2 ; i++){
		for(j = 0 ; j < c2 ; j++){
			matrixB[i][j] = 0;
		}
	}
 	
	int x , y;
	if(c1<=0 || c2<=0 || r1<=0 || r2<=0 || c1>=1000 || r1>=1000 || c2>=1000 || r2 >=1000 || c1!=r2)
    {
    	return -1;
	}
	if(c1 != r2){
		printf("Row of second matrix not equal to Col of second matrix:");
		exit(0);
	}

//reading values for matrix 1
	num=0;
	ch=fgetc(fptr);
	for( x=0;x<r1;x++)
	{
		for( y=0;y<c1;)
		{
			ch=fgetc(fptr);
			if(isspace(ch))
			{
				num = atoi(c);
				matrixA[x][y]=num;
				strcpy(c , empty);
				num=0;
				y++;
				
			}
			else
			{
				strncat(c , &ch , 1);
			}
		}
	}

//reading values for matrix 2

	num=0;
	ch=fgetc(fptr);
	for( x=0;x<r2;x++)
	{
		for( y=0;y<c2;)
		{
			ch=fgetc(fptr);
			if(isspace(ch))
			{
				num = atoi(c);
				matrixB[x][y]=num;
				strcpy(c , empty);
				num=0;
				y++;
				
			}
			else
			{
				strncat(c , &ch , 1);
			}
		}
		
	}
//initializing sample_result
	int**  sample_result ;
	sample_result = (int **) malloc(sizeof(int *) * r1);
	for(i=0; i<r1; i++)  
 		sample_result[i] = (int *)malloc(sizeof(int) * c2);

	num=0;
	ch=fgetc(fptr);
	for( x=0;x<r1;x++)
	{
		for( y=0;y<c2;)
		{
		ch=fgetc(fptr);
		if(isspace(ch))
		{
			num = atoi(c);
			sample_result[x][y]=num;
			strcpy(c , empty);
			num=0;
			y++;
		
		}
		else
		{
			strncat(c , &ch , 1);
		}
		}
	
	}
//initialize result
	int** result;
	result = (int **) malloc(sizeof(int *) * r1);
	for(i=0; i<r1; i++)  
 		result[i] = (int *)malloc(sizeof(int) * c2);
 		
 	for(i = 0 ; i < r1 ; i++){
		for(j = 0 ; j < c2 ; j++){
			result[i][j] = 0;
		}
	}

	clock_t start_time = clock();
	multiply_matrix_kji(r1 , c1, r2 , c2 , matrixA , matrixB , result);
	double elapsed_time = (double)(clock() - start_time) / CLOCKS_PER_SEC;
	double ms = elapsed_time * 1000;
	printf("\n");
    printf("Done in %.2f ms\n", ms);
    
//compare matrix multiplication result with actual matrix
	for( i=0;i<r1;i++)
	{
		for( j=0;j<c2;j++)
		{
			if(sample_result[i][j]!=result[i][j] )
			{
				printf("%d %d " , sample_result[i][j] , result[i][j]);
				printf("Output mismatched");
				return 0; 
			}
		}
				
	}
	printf("Testcase Passed");
	
	return 0;
}

int main(int argc , char* argv[]){
	char* c;
	c = argv[1];
	check_matrix(c); 
	
}
